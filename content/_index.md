---
title: Home
menu: "main"
linkTitle: "Home"
weight: 1
---

These notes and resources are all a work-in-progress. I write them as I teach each unit and each year group, so it is likely that you will find them incomplete or changing in many areas.

{{< coffee >}}

## Gitlab Repository

This website is generated using [Hugo](https://gohugo.io/) from a Gitlab repository which you can visit [over here](https://gitlab.com/Finnito/science).

### Reporting Issues

You can report issues/ideas/bugs in two ways:

1. Email me [lsf@cashmere.school.nz](mailto:lsf@cashmere.school.nz),
2. or [open an issue on Gitlab](https://gitlab.com/Finnito/science/issues).