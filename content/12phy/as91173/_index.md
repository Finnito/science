---
title: Electricity - AS91173
menu:
    main:
        parent: "12PHY"
katex: true
---

{{< slides >}}

## Lesson Plans


|         | Lesson 1        | Lesson 2               | Lesson 3                   | Lesson 4                   |
|---------|-----------------|------------------------|----------------------------|----------------------------|
| T2 W3  |                 | Static Electricity     | Conductors & Insulators    | Charge & Current           |
| T2 W4  | Voltage & Power | Resistance & Ohm's Law | Series & Parallel Circuits | Catch-Up                   |
| T2 W5  | Electric Fields | Electric Fields        | Electric Fields            | Electric Field Lines       |
| T2 W6  | Parallel Plates | Parallel Plates        | Parallel Plates            | __P:__ Circuit Building |
| T2 W7  | DC Circuits     | Voltage Divider        | Voltage Divider            | Voltage Divider            |
| T2 W8  | RH Slap Rule    | RH Slap Rule           | Magnetic Fields            | Magnetic Fields            |
| T2 W9  | Magnetic Fields | $F=BILsin(\theta)$      | $F=BILsin(\theta)$          | Generating AC              |
| T2 W10  | Generating AC   | Generating AC          | Lenz' Law in Solenoids     | Lenz' Law in Solenoids     |
| T2 W11  | Revision        | Revision               | Revision                   | Revision                   |
| T2 W12 | Revision        | __TEST__               |                            | _Hand Back_                |

## AS91173 Lesson Plans

## Week 1

__Google Meet:__ Wednesday 11:20am

This week is a short week so we will only have three lessons. We are starting our new topic, Electricity and Magnetism. We will spend the next 10 weeks (all of Term 2) on the topic, hopefully having our test in Week 12 of Term 2.

I have uploaded the homework book materials, all of the worksheets, the textbook activities and linked to the notes for this topic in Google Classroom so that you can find them whenever you need. As usual I will distribute the answers to the homework book as we go through the topic.

1. Static Electricity
    - Work through the static electricity notes
    - Make copies of diagrams
    - Answer the questions.
    - Complete questions 1-4 on _Worksheet 1 - Statics & Fields_

2. Conductors & Insulators
    - Work through the notes for charge and current, these notes include conductors and insulators.
    - Complete questions 1-5 on _Worksheet 3 - Voltage, Current and Resistance_

3. Charge & Current
    - Complete questions 1 and two from Activity 18A in the textbook
    - Compete Question 2a from the Homework Booklet

---

## Week 2

__Google Meets:__ Wednesday 11.20am, Thursday 2.10pm

1. Voltage & Power
    - Read through the voltage and power notes & answer the embedded questions

2. Resistance & Ohm's Law
    - Read through the notes & complete the embedded questions
    - Complete Q1-5 on Worksheet 3 if you found them hard last week and got stuck
    - Complete Q1 on Worksheet 5 - Circuits. This will require you to use all the equations you have learned so far!

3. Series & Parallel Circuits
    - Read through the notes & complete the embedded questions
    - Complete Question 7: 12V Lamps in the Homework Book - DC Electricity

---

## Week 3

Kia ora, everyone!

Going forwards I would like to see more of us on the scheduled Meets. From here on in we are entering new territory, so it is important that we all take the time to engage. My thanks to those of you who have been putting in the effort to attend regularly so far!

__Google Meets:__ Wednesday 11:20am, Thursday 2:10pm

This week we will be looking at the following:

1. Electric Fields (3 periods)
    - Find the notes in the usual area
    - Electric field strength
        - Worksheet 1 Question 11a-d
        - Homework book Electric Fields Question 4a, 4b
    - Electric potential energy
        - Textbook Activity 17A 1, 9a-b 
    - Voltage between parallel plates
        - Worksheet 2 Question 3a-d
2. Electric Field Lines (1 period)
    - Notes in the usual place
    - Textbook page 187 xfor extra notes if need be
    - Textbook Activity 17A Q4a-c
    - Worksheet 1 Q5, Q6

---

## Week 4

1. Electric Field Between Parallel Plates
    - Textbook Activity 17A Q10 as the starter
    - Recap notes on electric fields, voltage between parallel plates etc.
    - Work through questions that were unfinished from the previous week
2. Electric Field Between Parallel Plates
    - Work through unfinished questions
    - Assign 1x homework question due Monday 25th, P1.
3. Dangers of Electricity
4. Prac: Series & Parallel Circuits

---

## Week 5

1. DC Circuits
    - Review parallel and series circuits
    - Calculating resistance in series and parallel circuits
    - Start complex resistor networks
2. DC Voltage Divider
    - Complex resistor network calculations
    - Finding $I_{t}$ and voltages
3. Voltage Divider
    - Continue
4. Voltage Divider
    - Homework book past exam question

---

## Week 6

1. Voltage Divider
2. Right-Hand Slap Rule
3. Right-Hand Slap Rule
4. Magnetic Fields

---

## Week 7

1. Magnetic Fields
    - 
2. Magnetic Fields
3. F=BILsin(theta)
4. F=BILsin(theta)

---

## Week 8

1. EM Induction & Generating AC
2. EM Induction & Generating AC
3. EM Induction & Generating AC
4. Lenz' Law in Solenoids
    - Video to start: https://www.youtube.com/watch?v=pQp6bmJPU_0
    - Introductory notes on Lenz' Law in solenoids
    - Class demonstration using galvanometer, solenoid & magnet
    - Quick questions at the end of the powerpoint
    - Textbook: Activity 20A Qa
    - Homework Booklet: Q24

---

## Week 9

1. Revision
2. Revision
3. Revision
4. Revision

---

## Week 10

1. Revision
2. Test
3. 
4. Return
