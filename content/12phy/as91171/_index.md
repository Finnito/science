---
title: AS91171 - Mechanics
menu:
    main:
        parent: "12PHY"
---

NZQA Link: [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=AS91171](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=AS91171)

## Learning Outcomes

1. __Motion__
    1. Constant acceleration in a straight line
    2. Free fall under gravity
    3. Projectile motion
    4. Circular motion
2. __Force__
    1. Force components
    2. Vector addition of forces
    3. Unbalanced force and acceleration
    4. Equilibrium (balanced forces and torques)
    5. Centripetal force
    6. Force and extension of a spring
3. __Momentum & Energy__
    1. Momentum
    2. Change in momentum in one dimension and impulse
    3. Impulse and force
    4. Conservation of momentum in one direction
    5. Work
    6. Power and conservation of energy
    7. Elastic potential energy

{{< slides >}}

## Unit Plan

|          | Lesson 1              | Lesson 2               | Lesson 3                  | Lesson 4                 |
|:---------|:----------------------|:-----------------------|:--------------------------|:-------------------------|
| Week 1   | _Teacher Only Day_    | _Juniors Only_         | Maths License             | Speed & Acceleration     |
| Week 2   | Scalars & Vectors     | Vectors in 1D (Delta)  | _Waitangi Day_            | Vectors in 2D            |
| Week 3   | Vector Components     | Vector Revision        | Vector Test               | Kinematic Equations      |
| Week 4   | Kinematic Equations   | Kinematic Equations    | Newton's Laws             | Newton's Laws            |
| Week 5   | Newton's Laws         | __Mid-Topic Test #1__  | Projectile Motion         | Projectile Motion        |
| Week 6   | Projectile Motion     | Projectile Motion      | Projectile Motion         | Torque & Equilibrium     |
| Week 7   | Torque & Equilibrium  | Torque & Equilibrium   | Torque & Equilibrium      | Torque & Equilibrium     |
| Week 8   | Circular Motion       | Circular Motion        | Circular Motion           | __Mid-Topic Test #2__    |
| Week 9   | Momentum & Impulse    | Momentum & Impulse     | Momentum & Impulse        | Momentum & Impulse       |
| Week 10  | Momentum & Impulse    | Energy Conservation    | Elastic Potential Energy  | Work & Power             |
| Week 11  | __Revision__          | __TEST__               | _Hand Back Test_          | _Work on Test Mistakes_  |