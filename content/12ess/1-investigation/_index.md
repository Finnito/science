---
title: AS91187 - Meteorite Investigation
menu:
    main:
        parent: "12ESS"
---

NZQA Link: [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=AS91187](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=AS91187)

## Learning Outcomes

{{< slides >}}

## Unit Plan

|         | Monday                             | Wednesday                                             | Thursday         | Friday                                         |
|:--------|:-----------------------------------|:------------------------------------------------------|:-----------------|:-----------------------------------------------|
| Week 1  | _Teacher Only Day_                 | Meteors, Meteoroids & Meteorites                      | Asteroids        | Cocoa & Flour Practical                        |
| Week 2  | Impact Craters on Earth            | Moon, Mars & Earth Impacts                            | _Waitangi Day_   | Dinosaurs Extinction & Implications _(Leave)_  |
| Week 3  | Dinosaurs _(Leave)_                | Independent, Dependent & Control Variables _(Leave)_  | _Catch-Up_       | __ASSESSMENT__ _(Leave)_                       |
| Week 4  | Scientific Method & Egg Practical  | __ASSESSMENT__                                        | __ASSESSMENT__   | __ASSESSMENT__                                 |
| Week 5  | __ASSESSMENT__                     | __ASSESSMENT__                                        | _Athletics Day_  | __ASSESSMENT DUE__                             |