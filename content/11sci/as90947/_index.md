---
title: AS90947 - Chemical Reactions
menu:
    main:
        parent: "11SCI"
---

NZQA Link: [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=90947](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=90947)

## Learning Outcomes

1. To recall the symbols of the common elements in the periodic table
2. To recall what happens in a chemical reaction and evidence for it happening
3. To explain the difference between an element and a compound
4. To describe what happens in a __combination reaction__
5. To describe a precipitate and recall what happens in a __precipitation reaction__
6. To describe what happens in a __decomposition reaction__
7. To describe what happens in a __displacement reaction__
8. To recall the test for carbon dioxide using limewater
9. To predict and give examples of each type of chemical reaction
10. To write word equations to show a chemical reaction
11. To write symbol equations to show a chemical reaction with symbols of state (g, l s or ag)
12. To work out the formula of a compound using the swap and drop method
13. To balance symbol equations

{{< slides >}}

## Unit Plan

|          | Monday                             | Tuesday                       | Thursday                      | Friday                        |
| :------- | :--------------------------------- | :---------------------------- | :---------------------------- | :---------------------------- |
| T2 W10   | 1. Review of the Atom              | 2. P-Table & Electrons        | 3.  Compounds                 | __TOD__                       |
| T2 W11   | 4. Compounds & Electron Transfer   | 5. Writing Equations          | 6. Precipitation Reactions    | 7. Ionic Compounds            |
| T2 W12   | 8. Precipitation Reactions         | 9. Displacement Reactions     | 10. Combination Reactions     | 11. Displacement Practical    |
| T3 W1    | 12. Decomposition Reactions        | 13. Decomposition Practical   | 14. Gas Tests                 | 15. _Assessment Tips_         |
| T3 W2    | 16. _Catch-Up/Start Practice_      | 17. __Practice Assessment__   | __Practice Assessment__       | __Practice Assessment__       |
| T3 W3    | 18. _Revision & Feedback_          | 19. __Assessment__            | __Assessment__                | __Assessment__                |

### Lesson Plans

1. Review of the Atom
     - Intro to the unit
     - Review of the atom notes/worksheet + slides for backup
2. Periodic Table & Electrons
     - Worksheet
3. Ionic Compounds
     - Starter: Form into table groups and find out the name of each ion in a column and compile them on the board (project the table)
     - Copy other groups names onto your own table
     - Notes on atoms vs ions using diagrams and electron shell configurations
     - Notes on creating ionic compounds
     - Use whiteboards in table groups to make compounds as a quiz/speed game
4. Ionic Compounds
    - Finish quiz/speed game
    - Ionic compounds Quizlet
    - Writing formulae worksheet #1
5. Writing Equations
    - Video on ionic compounds
    - Talk about electron exchange
    - Notes on writing equations
6. Precipitation Reactions
    - Starter: writing word equation
    - Notes on precipitation reactions
    - Practice example forming lead iodide
7. Ionic Compounds
    - Need to do more practice with ionic compounds.
    - Starter: write a word and symbol equation for a precipitation reaction
    - Do CAR's 'knobs and holes' card match activity
    - Do ionic compound Battleships
8. Precipitation Reactions
    - Starter: Describe a precipitation reaction (equation, observations, and a word equation)
    - Practical: Precipitation Challenge
9. Displacement Reactions
    - Starter: Recap precipitation reactions
    - Notes and examples on displacement reactions
10. Combination Reactions
    - Starter: Displacement reactions question
    - Notes and examples on combination reactions
    - Education Perfect task on reactions?
11. Displacement Practical
12. Decomposition Reactions
    - Starter reviewing previous reaction types
    - Notes & video on decomposition reactions
    - Issue research assignment, explain it and give them the period to work on it
13. Decomposition Reactions Practical
    - https://www.riskassess.co.nz/risk_assessment/8323191
14. Gas Tests
	- Notes on gas tests
	- Gas tests experiments
	    - https://www.riskassess.co.nz/risk_assessment/8323197
	- Fill in notes on gas tests in the research document
15. Assessment Tips
	- Issue booklet for practice assessment
	- Go through how the assessment will work
	- Specify requirements for AME
	- Make predictions for practice assessment if time
