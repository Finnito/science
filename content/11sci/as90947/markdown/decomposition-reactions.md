---
title: Decomposition Reactions
subtitle: 11SCI - Chemical Reactions
author: Finn LeSueur
date: 2020
theme: finn
weight: 8
colortheme: dolphin
font-size: 35px
text-align: center
katex: true
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
- \usepackage{textcomp}
---

## Starter

1. Give the _general equation_ for:
    - A precipitation reaction
    - A displacement reaction
    - A combination reaction
2. Give an observation that you might make to indicate that each type of reaction has occured.

---

## Learning Outcomes

- To describe what happens in a decomposition reaction.

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/_Y1alDuXm6A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Decomposition Reactions

\begin{align*}
    AB \longrightarrow A + B
\end{align*}


A reaction where two ions are combined to form a compound. It is the reverse of __combination__ and a lot of energy is required for the reaction to occur.

---

### Example

Mercury (II) oxide is heated in a test tube. The solid is seen to change colour and a gas is given off.

1. If the gas is oxygen, how could you confirm this?
2. Write a word equation.
3. Write a symbol equation (with states) and balance it.

---

1. A glowing splint in the presence of oxygen gas, will reignite.
2. mercury oxide $\xrightarrow{\text{heat}}$ mercury + oxygen gas
2. $2HgO_{s} \xrightarrow{\text{heat}} 2Hg_{l} + O_{2g}$

---

