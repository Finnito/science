---
title: Energy Transformations
subtitle: 11SCI - Mechanics
author: Finn LeSueur
date: 2019
theme: finn
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter

Read page 55 and 56 of your sciPAD to remind yourself of some types of energy and how it is transformed between different types.

Then answer __Question 1 on page 59__.

---

In Physics we live in an idealised world where friction does not exist and energy is transformed with 100% efficiency.

This means that 100% of elastic potential energy is converted into kinetic energy, in the case of the rubber band.

It means that 100% of gravitational potential energy is converted into kinetic energy in the case of a sky diver.