---
title: Genetics Introduction
subtitle: 11SCI - Genetics
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
- \usepackage{textcomp}
---

# Learning Outcomes

- Define _species_
- Compare and contrast sexual and asexual reproduction

---

## Starter

__Think, pair and share:__

- recall MRS C GREN,
- define "species",
- discuss the importance of reproduction & the two ways to do it,
- and think of any advantages/disadvantages

---

__MRS C GREN__: Movement, Respiration, Sensitivity, Circulation, Growth, Reproduction, Excretion, Nutrition

---

__Species__: A group of organisms that can produce viable offspring

---

## Reproduction

There are two ways to reproduce: __sexual__ reproduction and __asexual__ reproduction.

---

### Sexual Reproduction

- Takes a lot of energy (effort)
- Involves sex cells (gametes)
- Greater genetic variation

---

#### What does sexual reproduction?

Any animal that you can think of that has different sexes and some flowers and plants.

---

### Asexual Reproduction

- Involves a single organism
- Produces clones (very little variation)
- Requires less energy

---

#### What does asexual reproduction?

Some plants, bacteria, some insects

---

#### Does anything do both?

Yes, aphids, slime moulds, sea anemones and some starfish are examples of organisms that can do both sexual and asexual reproduction.

---

### Exercise

Compare and contrast sexual and asexual reproduction. Watch this video to get some ideas and use page 30 in your sciPAD to get some more help.
